<cfscript>
    endpoint = "https://mandrillapp.com/api/1.0/messages/send-template.json";

    attachment = {
        "path": expandPath("./attachments/award-notification.pdf")
    };
    attachment["encoded"] = binaryEncode(fileReadBinary(attachment.path), "Base64");

    uglymess = fileRead(expandPath("./content/ugly-mess.html"));

    payload = {
        "key": createObject("java", "java.lang.System").getProperty("mandrill_api_key"),
        "template_name": "external-award-notification",
        "template_content": [],
        "message": {
            "merge": true,
            "track_clicks": true,
            "track_opens": true,
            "to": [
                {
                    "email": "mandrill.sendtest+mandrilldebug@gmail.com",
                    "name": "Mandrill Support"
                }
            ],
            "from_email": "donotreply@ecivis.com",
            "from_name": "eCivis Support",
            "merge_language": "mailchimp",
            "attachments": [
                {
                    "type": "application/pdf",
                    "content": attachment.encoded,
                    "name": "awardNotification.pdf"
                }
            ],
            "global_merge_vars": [
                {
                    "content": "My Program Solicitation Title",
                    "name": "solicitation_title"
                },
                {
                    "content": uglymess,
                    "name": "awardRecommendationText"
                },
                {
                    "content": "Your award has been finalized. You may manage your award in the eCivis Portal.",
                    "name": "NoteFromGrantor"
                },
                {
                    "content": "https://portal.ecivis.com/##/index/myAwards",
                    "name": "solicitationAward_link"
                }
            ]
        }
    };

    output = {
        "serializedPayload": serializeJSON(payload),
        "path": expandPath("./output/") & createUUID() & ".json"
    };

    fileWrite(output.path, output.serializedPayload);
    writeOutput("<p>Saved the JSON payload to #output.path#</p>");

    try {
        req = new Http(method="POST", url=endpoint, throwOnError=true);
        req.addParam(type="header", name="Content-Type", value="application/json");
        req.addParam(type="body", value=output.serializedPayload);

        resp = req.send().getPrefix();
        writeDump(var=resp);
    } catch (Any e) {
        writeDump(var=e, abort=true);
    }
</cfscript>
