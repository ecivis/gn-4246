#!/bin/bash

td=`mktemp -d -t gn-4246`

for f in "$@"; do
  base64 -i "$f" -o "${td}/${f}.enc"
  echo -n "Scanning $f"
  match=$(grep -i -o -E -e 'u\+[a-f0-9]{4}' "${td}/${f}.enc")
  if [ ! -z "$match" ]; then
    echo -e "\n  $match"
  else
    echo
  fi
done
