#!/bin/bash

body=$(cat <<EOF
{
  "template_content": [],
  "message": {
    "merge": true,
    "from_name": "eCivis Support",
    "from_email": "donotreply@ecivis.com",
    "attachments": [
      {
        "content": "enp6u+BAdcen",
        "type": "application/pdf",
        "name": "not-a-real-pdf.pdf"
      }
    ],
    "to": [
      {
        "email": "jlamoree@ecivis.com",
        "name": "Joseph Lamoree"
      }
    ],
    "merge_language": "mailchimp",
    "global_merge_vars": [
      {
        "content": "My Program Solicitation Title",
        "name": "solicitation_title"
      },
      {
        "content": "<p>Some HTML Content</p>",
        "name": "awardRecommendationText"
      },
      {
        "content": "Done. Good job.",
        "name": "NoteFromGrantor"
      },
      {
        "content": "https://portal.ecivis.com/#/index/myAwards",
        "name": "solicitationAward_link"
      }
    ]
  },
  "template_name": "external-award-notification",
  "key": "A Valid Key Goes Here"
}
EOF
)

curl -H 'Content-Type: application/json' -H 'Host: mandrillapp.com' \
  --data-binary "$body" \
  'https://mandrillapp.com/api/1.0/messages/send-template.json'
